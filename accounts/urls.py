from django.urls import path
from .views import login_user, logout_user, sign_up_user

urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", sign_up_user, name="signup"),
]

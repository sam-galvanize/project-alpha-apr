from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from .forms import LoginForm, SignUpForm


def login_user(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error(
                    "password", "Incorrect Password and/or Username!"
                )
                context = {"form": form}
                return render(request, "accounts/login.html", context)
    else:
        form = LoginForm()
        context = {"form": form}
        return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect("login")


def sign_up_user(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "Passwords do not match!")

                context = {"form": form}
                return render(request, "accounts/signup.html", context)

    else:
        form = SignUpForm()
        context = {"form": form}
        return render(request, "accounts/signup.html", context)

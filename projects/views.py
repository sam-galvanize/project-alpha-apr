from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


# Create your views here.
@login_required(redirect_field_name="login")
def list_projects(request):
    project_list = Project.objects.filter(owner=request.user)

    context = {"project_list": project_list}
    return render(request, "projects/list.html", context)


@login_required(redirect_field_name="login")
def show_project(request, id):
    project_object = Project.objects.get(id=id)

    context = {"project": project_object}
    return render(request, "projects/details.html", context)


@login_required(redirect_field_name="login")
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
        else:
            return redirect("create_project")
    else:
        form = ProjectForm()
        context = {"form": form}
        return render(request, "projects/create.html", context)
